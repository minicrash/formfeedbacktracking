import React, { useState } from "react"
import { Grommet, Box, Heading, Text, Image } from "grommet"
import Button from "./stories/components/Button/Button"
import Slider from "./stories/components/Slider/Slider"
import Select from "./stories/components/Select/Select"
import Answers from "./data/answers"
import Courbature from "./components/Courbature"

const App = props => {
  const [counter, setCounter] = useState(0)
  // let Sommeil = Answers.filter(answer => answer.categorie === "sommeil")
  // let Douleur = Answers.filter(answer => answer.categorie === "douleur")
  // let endcounter = Sommeil.length
  let endcounter = Answers.length
  // let endcounter = Douleur.length
  // console.log(Sommeil)
  return (
    <Grommet theme={theme}>
      {Answers.map(
        (answer, index) =>
          answer.active &&
          (counter === index && (
            <Box
              key={index}
              width={"large"}
              margin={{
                left: "auto",
                right: "auto",
                top: "large"
              }}
              responsive={true}
            >
              <Heading level="2" alignSelf="center" responsive={true}>
                {answer.answer}
              </Heading>

              <Box responsive={true}>
                {answer.type === "button" ? (
                  answer.data.map((button, index) => (
                    <Box pad="small" key={index}>
                      <Button
                        color="pink"
                        primary
                        label={button.value}
                        onClick={() => {
                          button.active = !button.active
                          setCounter(counter + 1)
                        }}
                      />
                    </Box>
                  ))
                ) : answer.type === "slider" ? (
                  <Box responsive={true}>
                    <Slider datas={answer.data} />
                    {index !== endcounter - 1 && (
                      <Button
                        margin={{ top: "medium" }}
                        label="Suivant"
                        onClick={() => setCounter(counter + 1)}
                      />
                    )}
                  </Box>
                ) : answer.type === "image" ? (
                  <Box responsive={true}>
                    <Image
                      fit="contain"
                      src={require(__dirname +
                        "/../public/assets/images/" +
                        answer.data[0].value)}
                    />
                    {index !== endcounter - 1 && (
                      <Button
                        label="Suivant"
                        onClick={() => setCounter(counter + 1)}
                      />
                    )}
                  </Box>
                ) : answer.type === "select" ? (
                  <Box responsive={true}>
                    <Courbature datas={answer.data} />
                    {index !== endcounter - 1 && (
                      <Button
                        label="Suivant"
                        onClick={() => setCounter(counter + 1)}
                      />
                    )}
                  </Box>
                ) : null}
              </Box>
            </Box>
          ))
      )}
    </Grommet>
  )
}
const theme = {
  global: {
    colors: {},
    font: {
      family: "Roboto",
      size: "14px",
      height: "20px"
    }
  }
}
export default App
