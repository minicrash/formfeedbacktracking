import React from "react"
import { Button } from "grommet"

const InputButton = props => {
  let { color, primary } = props
  let colorBtn = "#F8F8F8"
  switch (color) {
    case "pink":
      color = "#FD6FFF"
      colorBtn = "#444444"
      break
    case "green-light":
      color = "#6FFFB0"
      break
    case "blue-light":
      color = "#81FCED"
      break
    case "yellow-light":
      color = "#FFCA58"
      break
    case "green":
      color = "#00873D"
      break

    default:
      color = "#7D4CDB"
      colorBtn = "#444444"
      break
  }

  return (
    <Button
      primary={primary ? true : false}
      label={props.label}
      onClick={() => props.onClick()}
      color={color}
      style={(styles.textButton, { color: colorBtn })}
      {...props}
    />
  )
}
InputButton.defaultProps = {
  label: "Athlete"
}
const styles = {
  textButton: {
    fontFamily: "Roboto",
    size: "14px"
  }
}
export default InputButton
