import React, { useState } from "react"
import { RadioButtonGroup } from "grommet"

const Radio = (props, { name, options }) => {
  const [selected, setSelected] = useState()
  return (
    <RadioButtonGroup
      name={name}
      options={options}
      value={selected}
      onChange={event => {
        setSelected(event.target.value)
        props.handleRadioButton(event.target.value)
      }}
      {...props}
    />
  )
}
Radio.defaultProps = {
  name: "Douleurs",
  options: [
    { label: "Choc", value: "choc" },
    { label: "Courbature", value: "courbature" }
  ]
}
export default Radio
