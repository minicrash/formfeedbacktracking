import React, { useState } from "react"
import { Box, Select } from "grommet"

const InputSelect = props => {
  const [value, setValue] = useState("")
  return (
    <Select
      alignSelf="center"
      placeholder="Douleurs"
      options={props.datas}
      value={value}
      closeOnChange={true}
      onChange={({ value: nextValue }) => {
        setValue(nextValue)
        props.handleSelect(nextValue)
      }}
      disabledKey="active"
      labelKey="value"
      valueKey="value"
    />
  )
}
export default InputSelect
