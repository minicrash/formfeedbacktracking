import React, { useState } from "react"
import { Accordion, AccordionPanel, Box, Text } from "grommet"

class InputAccordion extends React.Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      panels: []
    }
  }
  static getDerivedStateFromProps(props, state) {
    let panels = props.data
    return { panels }
  }
  render() {
    const { panels } = this.state
    const { label } = this.props
    return (
      <Accordion>
        <AccordionPanel label={label}>
          {panels &&
            panels.length > 0 &&
            panels.map((panel, index) => (
              <Box key={index} style={{ cursor: "pointer" }}>
                <Text>{panel}</Text>
              </Box>
            ))}
        </AccordionPanel>
      </Accordion>
    )
  }
}
InputAccordion.defaultProps = {
  data: ["Douleurs 1", "Douleurs 2"]
}
export default InputAccordion
