import React, { useState } from "react"
import { RangeInput, Box, Text } from "grommet"

const Slider = props => {
  let defaultValue = parseInt(props.datas.length / 2)
  const [value, setValue] = useState({
    key: props.datas[defaultValue].key,
    label: props.datas[defaultValue].value
  })
  // const [value, setValue] = useState(5)
  const { datas } = props
  const { key, label } = value
  return (
    <Box>
      <Text alignSelf="center">{label}</Text>
      <RangeInput
        value={key}
        step={0.0001}
        max={datas.length - 1}
        onPointerUp={() => props.handleChange(value.key)}
        onChange={event => {
          let sliderValue = parseFloat(event.target.value)
          let data = datas.find(data => {
            let sliderValue_ = Number(parseInt(sliderValue))
            if (sliderValue_ === data.key) {
              return data
            }
          })
          setValue({ key: sliderValue, label: data.value })
        }}
      />
    </Box>
  )
}

Slider.defaultProps = {
  handleChange: value => console.log(value),
  datas: [
    {
      key: 0,
      value: 0,
      active: false
    },
    {
      key: 1,
      value: 1,
      active: false
    },
    {
      key: 2,
      value: 2,
      active: false
    },
    {
      key: 3,
      value: 3,
      active: false
    },
    {
      key: 4,
      value: 4,
      active: false
    },
    {
      key: 5,
      value: 5,
      active: false
    },
    {
      key: 6,
      value: 6,
      active: false
    },
    {
      key: 7,
      value: 7,
      active: false
    },
    {
      key: 8,
      value: 8,
      active: false
    },
    {
      key: 9,
      value: 9,
      active: false
    },
    {
      key: 10,
      value: 10,
      active: false
    }
  ]
}
export default Slider
