import React from "react"
import { Grommet, Box } from "grommet"
import { storiesOf } from "@storybook/react"
import { action } from "@storybook/addon-actions"
import { linkTo } from "@storybook/addon-links"

import { Button, Welcome } from "@storybook/react/demo"
import InputButton from "./components/Button/Button"
import InputSelect from "./components/Select/Select"
import InputAccordion from "./components/Accordion/Accordion"
import Radio from "./components/Radio/Radio"

storiesOf("Welcome", module).add("to Storybook", () => (
  <Grommet theme={theme}>
    <Welcome showApp={linkTo("Button")} />
  </Grommet>
))

storiesOf("Button", module)
  .add("with text", () => (
    <Button onClick={action("clicked")}>Hello Button</Button>
  ))
  .add("with some emoji", () => (
    <Button onClick={action("clicked")}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ))
  .add("grommet default buttons ", () => <InputButton primary />)
  .add("grommet pink buttons ", () => <InputButton primary color={"pink"} />)
  .add("grommet green light buttons ", () => (
    <InputButton primary color={"green-light"} />
  ))
  .add("grommet blue light buttons ", () => (
    <InputButton primary color={"blue-light"} />
  ))
  .add("grommet yelloz buttons ", () => (
    <InputButton primary color={"yellow-light"} />
  ))

storiesOf("Select", module)
  .add("default select", () => (
    <InputSelect data={["Small", "medium", "large"]} />
  ))
  .add("select json data", () => (
    <InputSelect
      data={[
        { key: 0, value: "--Tête--", active: true },
        { key: 1, value: "Tête", active: false },
        { key: 2, value: "Visage", active: false },
        { key: 3, value: "Cou", active: false },
        {
          key: 4,
          value: "--Membre supérieur--",
          active: true
        },
        { key: 5, value: "Epaule droite", active: false },
        { key: 6, value: "Epaule gauche", active: false },
        { key: 7, value: "Buste", active: true },
        { key: 8, value: "Poitrine", active: false },
        { key: 9, value: "Dos", active: false },
        { key: 10, value: "Lombaires", active: false },
        {
          key: 11,
          value: "--Membre inférieur--",
          active: true
        },
        { key: 12, value: "Quadriceps droit", active: false },
        { key: 13, value: "Quadriceps gauche", active: false }
      ]}
    />
  ))
  .add("select object", () => (
    <InputSelect
      data={[
        <InputButton primary color="blue-light" />,
        <InputButton label="Entrianement" primary color="blue-light" />
      ]}
    />
  ))

storiesOf("Accordion", module).add("default accordion", () => (
  <InputAccordion label="Courbature" />
))
storiesOf("Radio Button", module).add("default radio button", () => (
  <Box>
    <Radio />
  </Box>
))
const theme = {
  global: {
    colors: {},
    font: {
      family: "Roboto",
      size: "14px",
      height: "20px"
    }
  }
}
