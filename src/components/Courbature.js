import React, { useState } from "react"
import { Box, Heading } from "grommet"
import InputSelect from "../stories/components/Select/Select"
import Slider from "../stories/components/Slider/Slider"
import InputButton from "../stories/components/Button/Button"
import InputAccordion from "../stories/components/Accordion/Accordion"
import Radio from "../stories/components/Radio/Radio"
const Courbature = props => {
  const [values, setValue] = useState([])
  const [counter, setCounter] = useState(0)
  const [intensity, setIntensity] = useState()
  const [localize, setLocalize] = useState()
  const [typeDouleur, setTypeDouleur] = useState()
  return (
    <Box>
      <InputAccordion label={"Douleurs"} data={values} />
      <InputSelect
        datas={props.datas}
        handleSelect={value => setLocalize(value)}
      />
      <Heading level="3" alignSelf="center">
        Intensité :
      </Heading>
      <Slider handleChange={value => setIntensity(value)} />
      <Radio
        alignSelf="center"
        handleRadioButton={value => setTypeDouleur(value)}
      />
      <InputButton
        primary
        color="pink"
        label="Ajouter"
        onClick={() => {
          let c = counter + 1
          setCounter(c)
          setValue([...values, `Douleur ${c}`])
          console.log(intensity, localize, typeDouleur)
        }}
      />
    </Box>
  )
}

Courbature.defaultProps = {
  title: "Où se situe ta courbature ?"
}
export default Courbature
